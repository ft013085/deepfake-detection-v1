import numpy as np
import tensorflow as tf
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.preprocessing import image
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D
from tensorflow.keras.preprocessing.image import img_to_array
from PIL import Image
import requests
from io import BytesIO
import matplotlib.pyplot as plt

# Load MobileNetV2 for feature extraction
base_model = MobileNetV2(weights='imagenet', include_top=False, input_shape=(256, 256, 3))

# Add custom layers on top of MobileNetV2
x = base_model.output
x = GlobalAveragePooling2D()(x)
x = Dense(512, activation='relu')(x)
predictions = Dense(1, activation='sigmoid')(x)

# Create the full model
model = Model(inputs=base_model.input, outputs=predictions)

# Unfreeze the last few layers of the base model for fine-tuning
for layer in base_model.layers[-20:]:
    layer.trainable = True

# Compile the model
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Function to preprocess the input image from URL
def preprocess_image_from_url(image_url):
    response = requests.get(image_url)
    img = Image.open(BytesIO(response.content))
    img = img.resize((256, 256))
    img = img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = tf.keras.applications.mobilenet_v2.preprocess_input(img)
    return img

# Function to predict if the image is a deepfake
def predict_deepfake(image_url):
    img = preprocess_image_from_url(image_url)
    prediction = model.predict(img)
    return 'Fake' if prediction > 0.5 else 'Real'

# Example usage
if __name__ == "__main__":
    # URL of the image to be tested
    image_url = 'https://ichef.bbci.co.uk/ace/standard/480/cpsprodpb/12E3F/production/_132957377_obama_getty.jpg'
    
    # Load and display the image
    response = requests.get(image_url)
    img = Image.open(BytesIO(response.content))
    plt.imshow(img)
    plt.axis('off')
    plt.show()

    # Predict and print the result
    result = predict_deepfake(image_url)
    print(f"The image is: {result}")

    # Train the model (example training loop)
    # This assumes you have a training and validation generator ready
    # Note: Replace 'train_generator' and 'validation_generator' with your data generators
    # model.fit(train_generator, epochs=10, validation_data=validation_generator)
